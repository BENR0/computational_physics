#This code can be found at https://bitbucket.org/Roesnerb/computational_physics/src/
#Author: Benjamin Roesner

import numpy as np
import matplotlib.pyplot as plt

def newton(fun, ini_x, iter_max, epsilon):
    #fun: function input
    #ini_x: initial x value
    #iter_max: maximum number of iterations
    #epsilon: convergence criterion

    #init iteration count
    num_iter = 0
    #step size for derivative
    step_size = 0.1
    x = ini_x
    x_prev = x + 20 * epsilon

    while (abs(x) > epsilon) and (num_iter < iter_max):
        num_iter += 1
        #evaluate function
        f_eval = fun(x)
        #evaluate derivative of function
        dfdx = (fun(x + step_size) - fun(x)) / step_size
        #save iteration result for condition matching
        x_prev = x
        #calculate x
        x = x_prev - f_eval / dfdx
    
    return x, num_iter

def f(z, a = 0.32 + 1j * 1.64):
    return z**3 + (a - 1) * z - a

#max iterations
max_iter = 300
#epsilon
epsilon = 10e-2
#array size
n = 500
#window to calculate 
win = 1.0
x_min = -1 * win
x_max =  win
y_min = -1 * win
y_max = win

#init array for data
data = np.zeros(shape=(n,n))

for ix in range(n):
    x = x_min + ix * (x_max - x_min)/(n-1)
    for iy in range(n):
        y = y_min + iy * (y_max - y_min)/(n-1)

        z = complex(x, y)

        data[ix, iy] = newton(f, z, max_iter, epsilon)[1]

#fig1 = plt.gcf()
image = plt.imshow(data)
plt.colorbar(image, orientation = "horizontal")
plt.show()
#fig1.savefig("test.png")

import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate

#FitzHugh-Naguno equations
def FitzHugh(t, y, args):
    a, b, c, I = args
    #a = 0.7
    #b = 0.8
    #c = 0.08

    V = y[0]
    W = y[1]

    dydt = np.zeros(2)
    dydt[0] = V - V**3 / 3 - W + I
    dydt[1] = c * (V + a - b * W)
    return dydt



#time ranges for integration
t_start = 0.0
t_end = 50.0
delta_t = 0.01
#number of steps (plus one for initial condition)
n_steps = np.floor((t_end - t_start)/delta_t) + 1

#initial values
V_zero = 0.5
W_zero = 0

#Parameters of the equations
a = 0.7
b = 0.8
c = 0.08

I = np.linspace(0, 0.5, 8)

#vectors for results
V_res = np.zeros((n_steps, len(I)))
W_res = np.zeros((n_steps, len(I)))

#integrate for diffe rent values of I
for a in range(len(I) - 1):
    #set integrator
    r = integrate.ode(FitzHugh).set_integrator("vode", method = "bdf")
    #r = integrate.ode(FitzHugh).set_integrator("dopri5")

    #set initial values to ODE
    r.set_initial_value([V_zero, W_zero], t_start)
    r.set_f_params((a, b, c, I[a]))
    time = np.zeros((n_steps, 1))
    #integrate
    step = 1
    while r.successful() and step < n_steps:
        r.integrate(r.t + delta_t)
        V_res[step, a] = r.y[0]
        W_res[step, a] = r.y[1]
        time[step] = r.t
        step += 1

    #plot results
    plt.rcParams["figure.figsize"] = [12, 6]
    #first panel
    ax1 = plt.subplot(2,1,1)
    plt.subplots_adjust(right = 0.7)
    ax1.set_ylabel("V")
    plt.plot(time, V_res[:, a], label = "I={0}".format(I[a]))
    #second panel
    ax2 = plt.subplot(2,1,2)
    ax2.set_ylabel("W")
    ax2.set_xlabel("Time")
    plt.plot(time, W_res[:, a], label = "I={0}".format(I[a]))
    plt.legend(loc = (1.1, 0.5))

plt.show()


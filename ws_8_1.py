import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate

#Toda oscillator
def TodaOsci(t, y, args):
    #parameters
    rr, w, a = args
    #r = args[0]
    #w = args[1]
    #a = args[2]
    #r = 0.1
    #w = 1.0
    #a = 1.0
    
    x1 = y[0]
    x2 = y[1]

    dydt = np.zeros(2)
    dydt[0] = x2 * (1 + x1)
    dydt[1] = -1 * rr * x2 - x1 + a * np.cos(w * t)
    return dydt

# set time range
t_start = 0.0
t_final = 100.0
delta_t = 0.01
#calculate number of steps
num_steps = np.floor((t_final - t_start)/delta_t) + 1

#set initial values
x1_zero = 1.0
x2_zero = 1.0

#Parameters
rr = 0.1
w = 1.0
h = np.linspace(0.0, 1.0, 5)

#init vectors to store results
x1_res = np.zeros((num_steps, len(h)))
x2_res = np.zeros((num_steps, len(h)))

#integrate for different values of a
for i in range(len(h) - 1):
    #set integrator
    r = integrate.ode(TodaOsci).set_integrator("vode", method = "bdf")

    #set initial values and parameters
    r.set_initial_value([x1_zero, x2_zero], t_start)
    r.set_f_params((rr, w, h[i]))

    #init time vector
    time = np.zeros((num_steps, 1))
    
    #integrate
    step = 1
    while r.successful() and step < num_steps:
        r.integrate(r.t + delta_t)
        x1_res[step, i] = r.y[0]
        x2_res[step, i] = r.y[1]
        time[step] = r.t
        step += 1

    #plot results
    plt.rcParams["figure.figsize"] = [12, 6]
    #first panel y vx time
    ax1 = plt.subplot(2,1,1)
    plt.subplots_adjust(right = 0.7)
    ax1.set_ylabel("x1 and x2")
    ax1.set_xlabel("Time")
    plt.plot(time, x1_res[:, i], label = "a={0}".format(h[i]))
    #second panel x1 vs x2
    ax2 = plt.subplot(2,1,2)
    ax2.set_ylabel("x2")
    ax2.set_xlabel("x1")
    plt.plot(x1_res[:, i], x2_res[:, i], label = "a={0}".format(h[i]))
    plt.legend(loc = (1.1, 0.5))

plt.show()

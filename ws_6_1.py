#integration of scattering angle
#author: Benjamin Roesner

import numpy as np
import matplotlib.pyplot as plt

#def denominator
def denominator(b, r, E):
    return r**2 * (1 - b**2 / r**2 + np.exp(-r) /E)**(0.5)

#plotting of denominator
x = np.linspace(2, 100, 1)

y = denominator(1, x, 10)

plt.plot(x,y)
plt.show()


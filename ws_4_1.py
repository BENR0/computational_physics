#/usr/bin/env python
#
###################################################################
#Description: Computes equilibrium positions for N particle system
#             trapped in harmonic potential
#Author: Benjamin Roesner
##################################################################
import numpy as np
import matplotlib.pyplot as plt


def newton(fun, ini_x, iter_max, epsilon):
    #fun: function input
    #ini_x: initial x value
    #iter_max: maximum number of iterations
    #epsilon: convergence criterion

    #init iteration count
    num_iter = 0
    #step size for derivative
    step_size = 0.1
    x = ini_x
    x_prev = x + 20 * epsilon

    while (abs(x) > epsilon) and (num_iter < iter_max):
        num_iter += 1
        #evaluate function
        f_eval = fun(x)
        #evaluate derivative of function
        dfdx = (fun(x + step_size) - fun(x)) / step_size
        #save iteration result for condition matching
        x_prev = x
        #calculate x
        x = x_prev - f_eval / dfdx
    
    return x, num_iter

#force of two particles
def f2(x): 
    return -2*x + 1 / x**2

#force of three particles on a line (one in the middle)
def f3(x): 
    return -3*x + 2 / x**2 + 1 / (2*x**2)

def fn(x, npart = 6):
    return -x + 1/(2*x**2) + 2/(x**2 * np.sin(360/npart)) + 2/(x**2 * np.sin(360*2/npart)) + npart/x**2

#max iterations
max_iter = 3000
#epsilon
epsilon = 10e-2
#array size
n = 5000
#window to calculate 
win = 20.0
x_min = 0
x_max =  win

x = np.linspace(0.1, 20, num = n)
y2 = f2(x)
y3 = f3(x)
y6 = fn(x)
plot = plt.plot(x,y6)
plt.plot(x,y2)
plt.plot(x,y3)
plt.show()

res2 = newton(f2, 1, max_iter, epsilon)
res3 = newton(f3, 1, max_iter, epsilon)
res6 = newton(fn, 2, max_iter, epsilon)
print(res2, res3, res6)



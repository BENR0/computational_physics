epsilon = 1.0

while (1 + epsilon) - 1 > 0.0:
    print(epsilon)
    epsilon /= 2

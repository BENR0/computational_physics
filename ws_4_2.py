#/usr/bin/env python
#
###################################################################
#Description: Computes LU decomposition of matrix + and solution to 
#             Problem Ax = b
#Author: Benjamin Roesner
##################################################################
import numpy as np
import matplotlib.pyplot as plt

def lu(matrix):
    #check if matrix is square
    if not matrix.shape[0] == matrix.shape[1]:
        raise ValueError("Matrix ist not square.")
        
    #size of matrix
    n = matrix.shape[0]

    LU = np.zeros((n,n))
    LU[:] = A

    for i in range(n-1):
        for j in range(i+1,n):
            #calculate factor for row
            LU[j,i] = LU[j,i] / LU[i,i]
            #build difference of rows
            LU[j,i+1:] = LU[j,i+1:] - LU[j,i] * LU[i,i+1:] 

    return LU


def solvelu(matrix, resultvector):
    n = len(A)

    test = resultvector
    #solve from top down (lower triangular matrix)
    y = np.zeros((n,1), dtype = "float128")
    for i in range(0, n-1, 1):
        y[i,0] = test[i] / (matrix[i,i] / matrix[i,i])
        for k in range(0, i+1, 1):
           test[i+1] = test[i+1] -  matrix[i+1,k] * y[i,0]

    #solve from bottom up (upper triangular matrix)
    x = np.zeros((n,1), dtype = "float128")
    for i in range(n-1, -1, -1):
        x[i,0] = y[i,0] / matrix[i,i]
        for k in range(i-1, -1, -1):
            y[k,0] -= matrix[k,i] * x[i,0]

    return x


A = np.matrix("1.2969 0.8648 ; 0.2161 0.1441")
x = np.matrix("0.9911 ; -0.4870")
b = np.matrix("0.8642 ; 0.1440")

B = np.matrix("1 2 3 4 5 ; 6 7 8 9 10 ; 11 12 13 14 15 ; 16 17 18 19 20 ; 21 22 23 24 25")

Residual = np.dot(A,x) - b

#output = open("ws_4_2.txt", "wb")
print("Worksheet 4")
print("==================================================================")
print("Author: Benjamin Roesner\n")
print("Solution 4.2:\n")

print("Residual:")
print(Residual)
print("\n")

result = lu(A)
print("LU decomposition:")
print(result)

print("\n")
print("numpy solution:")
numpyres = np.linalg.solve(A, b)
print(numpyres)

print("\n")
lgsres = solvelu(result, b)
print("LU solution")
print(lgsres)

#output.close()

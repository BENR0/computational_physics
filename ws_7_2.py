import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def rk(x, y, fun, xmax,  h, order = 4):
    #fourth order runge kutta
    def rk4(fun, x, y, h):
        k0 = h*fun(x, y)
        k1 = h*fun(x + h/2.0, y + h/2.0 * k0)
        k2 = h*fun(x + h/2.0, y + h/2.0 * k1)
        k3 = h*fun(x + h, y + h * k2)

        return (k0 + 2.0*k1 + 2.0*k2 + k3)/6

    def rk2(fun, x, y, h):
        k0 = h*fun(x, y)
        k1 = h*fun(x + h/2.0, y + 0.5*k0)
        return k1
    
    resx = []
    resy = []
    #append initial values
    resx.append(x)
    resy.append(y)

    #calculate
    while x < xmax:
        if order == 4:
            y = y + rk4(fun, x, y, h)
        elif order == 2:
            y = y + rk2(fun, x, y, h)

        x = x + h
        resx.append(x)
        resy.append(y)

    return np.array(resx), np.array(resy)

###########
#testing function
#x = 0.0
#xmax = 2.0
#y = np.array([0.0, 1.0])
#h = 0.2

#DGL (from kiusalaas - Numerical method in engineering p.271)
#y'' = -0.1 * y' - x ; y(0) = 0 ; y'(0) = 1
#it follows with y_0 = y and y_1 = y'
#F(x,y)
#y' = y_1
#y_1' = -0.1 * y_1 - x
#def F(x,y):
    #F = np.zeros(3)
    #F[0] = y[1]
    #F[1] = -0.1*y[1] - x
    #return F

#xnum, ynum = rk(x, y, F, xmax, h, order = 2)
##yexact = np.log(1/np.sin(xnum) - 1/np.tan(xnum)) + 0.604582
#yexact = 100.0*xnum - 5.0*xnum**2 + 990.0*(np.exp(-0.1*xnum) - 1.0)

#plt.plot(xnum, ynum, label = "numerical")
#plt.plot(xnum, yexact, label = "exact")
#plt.legend()
#plt.show()
###########

#Lorenz system
sigma = 10.0
b = 8.0/3.0
R = 18.0
def F(x,y):
    #x is independent variable (eg. time)
    F = np.zeros(3)
    F[0] = sigma * (y[1] - y[0])
    F[1] = R * y[1] - y[0] - y[1] * y[2] 
    F[2] = y[0] * y[1] - b * y[2]
    return F

#init initial conditions
#starttime
x = 0.0
#endtime
xmax = 10.0
#y values
in1 = 1.0
in2 = 1.0
in3 = 0.5
#value array
y = np.array([in1, in2, in3])
#step size
h = 0.001

#different starting conditions
y1 = np.array([in1 + 1*10**-11, in2, in3])

#integration
#ynum = np.asarray([rk(x, yi, F, xmax, h, order = 4) for yi in yrand])
xnum, ynum = rk(x, y, F, xmax, h, order = 4)
xnum, ynum1 = rk(x, y1, F, xmax, h, order = 4)

ydiff = ynum1[:,0] - ynum[:,0]
plt.plot(xnum, ydiff)
plt.show()

##plotting of solution
#plt.plot(xnum, ynum[:,0], label = "x")
#plt.plot(xnum, ynum[:,1], label = "y")
#plt.plot(xnum, ynum[:,2], label = "z")
#plt.legend()
#plt.show()

#3d plot
fig = plt.figure()
ax = fig.gca(projection="3d")
#choose different colors for each trajectory
#colors = plt.cm.jet(np.linspace(0, 1, ntrajectories))
ax.plot(ynum[:,0], ynum[:,1], ynum[:,2], label = "v1")
ax.plot(ynum1[:,0], ynum1[:,1], ynum1[:,2], label = "v1 + delta")
ax.legend()
plt.show()

##########


#def harmonicoszi(x,y):
    #return 1j*0.5*y

#x = 0.0
#xmax = 30.0
#y = 1.0
#h = 0.1

#xnum, ynum = rk(x, y, harmonicoszi, xmax, h, order = 4)
#xnum2, ynum2 = rk(x, y, harmonicoszi, xmax, h, order = 2)

#plt.plot(xnum, ynum, label = "rk4")
#plt.plot(xnum2, ynum2, label = "rk2")
#plt.legend()
#plt.show()

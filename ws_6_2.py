import numpy as np
import matplotlib.pyplot as plt

def rk(x, y, fun, xmax,  h, order = 4):
    #fourth order runge kutta
    def rk4(fun, x, y, h):
        k0 = h*fun(x, y)
        k1 = h*fun(x + h/2.0, y + h/2.0 * k0)
        k2 = h*fun(x + h/2.0, y + h/2.0 * k1)
        k3 = h*fun(x + h, y + h * k2)

        return (k0 + 2.0*k1 + 2.0*k2 + k3)/6

    def rk2(fun, x, y, h):
        k0 = h*fun(x, y)
        k1 = h*fun(x + h/2.0, y + 0.5*k0)
        return k1
    
    resx = []
    resy = []
    #append initial values
    resx.append(x)
    resy.append(y)

    #calculate
    while x < xmax:
        if order == 4:
            y = y + rk4(fun, x, y, h)
        elif order == 2:
            y = y + rk2(fun, x, y, h)

        x = x + h
        resx.append(x)
        resy.append(y)

    return np.array(resx), np.array(resy)

###########
#testing function
x = 0.0
xmax = 2.0
y = np.array([0.0, 1.0])
h = 0.2

def F(x,y):
    F = np.zeros(2)
    F[0] = y[1]
    F[1] = -0.1*y[1] - x
    return F

xnum, ynum = rk(x, y, F, xmax, h, order = 2)
#yexact = np.log(1/np.sin(xnum) - 1/np.tan(xnum)) + 0.604582
yexact = 100.0*xnum - 5.0*xnum**2 + 990.0*(np.exp(-0.1*xnum) - 1.0)


plt.plot(xnum, ynum, label = "numerical")
plt.plot(xnum, yexact, label = "exact")
plt.legend()
plt.show()
##########


def harmonicoszi(x,y):
    return 1j*0.5*y

x = 0.0
xmax = 300.0
y = 1.0
h = 0.0001

xnum, ynum = rk(x, y, harmonicoszi, xmax, h, order = 4)
xnum2, ynum2 = rk(x, y, harmonicoszi, xmax, h, order = 2)

plt.plot(xnum, ynum, label = "rk4")
plt.plot(xnum2, ynum2, label = "rk2")
plt.legend()
plt.show()

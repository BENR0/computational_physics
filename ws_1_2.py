import csv
import math

vcos = math.cos(1.0)

vsin = - math.sin(1.0)

def v1(vsin, vcos, n):
    h = 2.0**(-n)
    abl = vsin - (math.cos(1.0+h) - vcos)/h
    return(abl)

def v2(vsin, vcos, n):
    h = 2.0**(-n)
    abl = vsin - (vcos - math.cos(1.0-h))/h
    return(abl)

def v3(vsin, n):
    h = 2.0**(-n)
    abl = vsin -  (math.cos(1.0+h) - math.cos(1.0-h))/(2.0*h)
    return(abl)


tfile = open('output/ws_1_2_ableitung.csv', 'w')
outfile = csv.writer(tfile)

for n in range(1, 100):
    row = [n, v1(vsin, vcos,n), v2(vsin, vcos,n), v3(vsin, n)]
    outfile.writerow(row)

tfile.close()
